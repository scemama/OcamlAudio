open Core

let () = 
  Parallel.set_nproc 128;
  let a =
    Array.init 127 ~f:(fun i -> i)
  in
  Parallel.Array.map ~f:(fun x -> 2*x) a
  |> Array.iter ~f:(fun x -> Printf.printf "%d\n" x)

