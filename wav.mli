(** Abstraction of wav files. This module requires the MM package.  *)

(** Number of frames (samples). Needs to be non-negative. *)
module Num_frames :
  sig
    type t
    val of_int64  : int64 -> t
    val to_int64  : t -> int64
    val of_int    : int -> t
    val to_int    : t -> int
    val to_string : t -> string
  end

(** Sample rate in Hertz. *)
module Sample_rate :
  sig
    type t
    val of_int : int -> t
    val to_int : t -> int
    val to_string : t -> string
  end

(** Number of channels (1,2 or 4) *)
module Channels :
  sig
    type t = MONO | STEREO | QUAD
    val of_int : int -> t
    val to_int : t -> int
    val to_string : t -> string
  end

(** Contains the audio data in an array of float tuples. The type of 
    the tuples depend on the number of channels. *)
module Audio_data :
  sig
    type t =
      MONO   of float array
    | STEREO of float array * float array
    | QUAD   of float array * float array * float array * float array
    val of_float_arrays : channels:Channels.t -> float_arrays:float array array -> t
    val to_float_arrays : t -> float array array
    val empty : channels:Channels.t -> t
    (** Creates empty data *)
  end

(** Abstract representation of a wav file. The wav file is always read
    in memory. *)
module File :
  sig
    type t = {
      name : string;
      sample_rate : Sample_rate.t;
      channels : Channels.t;
      frames : Num_frames.t;
      data : Audio_data.t;
    }

    val read : filename:string -> t option
    (** Read a wav file from the file system. *)

    val read_exn : filename:string -> t
    (** Read a wav file from the file system. Raise an exception on error *)

    val write : t -> unit
    (** Write a wav file to the file system *)

    val print_info : t -> unit
    (** Displays metadata describing the wav file *)

    val get_name : t -> string
    (** Extracts the file name from the {!File} *)

    val set_name : t -> name:string -> t
    (** Returns a new {!File} with the file name replaced *)

    val get_data : t -> Audio_data.t
    (** Extracts the {!Audio_data} from the {!File} *)

    val set_data : t -> data:Audio_data.t -> t
    (** Returns a new {!File} with the {!Audio_data} replaced *)
  end

