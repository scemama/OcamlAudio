let overlap ~reference data =
  let reference = 
    match reference with
    | Wav.Audio_data.MONO a ->
        let result = 
          Array.map abs_float a
        in
        Array.sub result 0 ((Array.length result) / 4) 
    | _ -> failwith "Expected a MONO reference file"
  and data = 
    match data with
    | Wav.Audio_data.MONO a ->
        Array.map abs_float a
    | _ -> failwith "Expected a MONO data file"
  in
  let ref_size =
    Array.length reference
  and data_size =
    Array.length data
  in
  let matching j =
    if (data_size-j <= ref_size) then
      0.
    else
      let rec loop ~accu = function
        | 0 -> accu +. (reference.(0) *. data.(j))
        | i -> 
            let new_accu = 
              accu +. reference.(i) *. data.(j+i)
            in
            loop ~accu:new_accu (i-1)
      in
      loop ~accu:0. (ref_size-1)
  in
  Array.init data_size (fun i -> i)
  |> Parallel.Array.map ~f:matching 



let smooth ~width data =
  let data_size =
    Array.length data
  in
  let result = 
    Array.make data_size 0.
  in
  let n = 
      1. /. (Float.of_int width)
  in
  let rec loop val_old i_right i = function
    | -1 -> ()
    | i_left -> 
        let val_new = 
          val_old -. data.(i_right) +. data.(i_left)
        in
        result.(i) <- val_new *. n;
        loop val_new (i_right-1) (i-1) (i_left-1) 
  in
  let k = 
    data_size - 1
  in
  begin
    try
      loop 0. k (k-width/2) (k-width)
    with Invalid_argument _ -> ()
  end;
  result


let derivative ~width data =
  let size = 
    (Array.length data)
  in
  Array.init size (function 
    | i when i < width -> data.(i) -. data.(0)
    | i when i >= size - width -> data.(size-1) -. data.(i)
    | i -> data.(i+width) -. data.(i-width)
  )


let find_clicks ~reference ~width data =
  let ov =
    overlap ~reference data
  in
  let sm =
    smooth ~width ov
  in
  let size = 
    (Array.length sm)
  in
  let de = 
    derivative ~width sm
  in
  let de2 = 
    derivative ~width de
  in
  let maxval a =
    Array.fold_left max 0. a
  and minval a =
    Array.fold_left min 0. a
  in
  let max_sm = 
    (maxval sm) *. 0.9
  and min_de2 =
    (minval de2) *. 0.9
  in
  Printf.printf "max_sm  %f\n" max_sm;
  Printf.printf "min_de2 %f\n" min_de2;
  let rec loop accu = function
    | -1 -> accu
    | i -> 
        let new_accu = 
          if (    (sm.(i) > max_sm) 
                && (de2.(i) < min_de2)
                && (de.(i) < 0.)
                && (de.(i-1) > 0.)
              ) then
                i :: accu
          else
            accu
        in
        loop new_accu (i-1)
  in
  let approx_pos = 
    loop [] (size-1)
  in
  let find_max i =
    let i_left = 
      if ((i+width) > (size-1)) then
        size-width
      else
        max (i-width) 0
    in
    let rec loop accu jmax = function
    | -1 -> jmax
    | j ->
        let new_accu, new_jmax =
          if (ov.(i_left+j) > accu) then
            ov.(i_left+j), (i_left+j)
          else
            accu, jmax
        in
        loop new_accu new_jmax (j-1)
    in
    loop 0. 0 width
  in
  List.map find_max approx_pos


