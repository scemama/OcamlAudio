val set_nproc : int -> unit

val spawn : 
  f:('a -> 'b) -> 'a -> (unit -> 'b)


module Array : sig

  val generic_map :
    map_func:(('a -> 'b) -> 'a array -> 'b array) ->
    f:('a -> 'b) ->
    'a array ->
    'b array

  val map:
    f:('a -> 'b) ->
    'a array ->
    'b array

end
