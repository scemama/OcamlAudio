Ocaml Wav tools
===============

This repository contains a set of tools to manipulate wav files in Ocaml.

Requirements:

* ``mm`` : ``opam install mm``
