open Core


let run left_filename right_filename out_filename =

  let empty_file name sample_rate =
    Wav.File.( { name = name ;
                 channels = Wav.Channels.MONO ;
                 frames = Wav.Num_frames.of_int 0 ;
                 sample_rate = sample_rate ;
                 data = Wav.Audio_data.empty Wav.Channels.MONO ;
              } )
  in
  let left_file, right_file, sample_rate  = 
    match Wav.File.(read left_filename, read right_filename) with
    | (Some left_file, Some right_file) -> 
        let sample_rate =
          if Wav.File.(left_file.sample_rate = right_file.sample_rate) then
            left_file.Wav.File.sample_rate
          else
            failwith "Sample rates don't match"
        in
        (left_file, right_file, sample_rate)
    | (None, Some right_file) -> 
        let sample_rate = 
          right_file.Wav.File.sample_rate
        in
        let left_file = 
          empty_file left_filename sample_rate
        in
        (left_file, right_file, sample_rate)
    | (Some left_file, None) -> 
        let sample_rate = 
          left_file.Wav.File.sample_rate
        in
        let right_file = 
          empty_file right_filename sample_rate
        in
        (left_file, right_file, sample_rate)
    | (None, None) -> failwith "Both input files are empty"
  in

  let data = 
    let left = 
      Wav.File.get_data left_file
      |> Process.convert_to_mono
    and right = 
      Wav.File.get_data right_file
      |> Process.convert_to_mono
    in
    Process.stereo_of_two_mono ~left ~right
  in

  let nframes =
    match data with
    | Wav.Audio_data.STEREO (l,r) -> Wav.Num_frames.of_int @@ Array.length l
    | _ -> assert false
  in
      


  let new_file =
   Wav.File.({ name = out_filename ;
                channels = Wav.Channels.STEREO ;
                frames = nframes ;
                sample_rate = sample_rate ;
                data = data
              })
  in
  Wav.File.write new_file



let () = 
  let doc = 
    "Converts two mono wav files into one stereo file."
  in
  let spec =
    let open Command.Spec in
    empty
    +> flag "l" (required string)
       ~doc:("<WAV_FILE> name of the left channel")
    +> flag "r" (required string)
       ~doc:("<WAV_FILE> name of the right channel")
    +> flag "o" (required string)
       ~doc:("<WAV_FILE> name of the stereo output file")
  in
  Command.basic_spec
  ~summary:doc
  ~readme:(fun () -> "")
  spec
  (fun l r o () -> run l r o)
  |> Command.run


