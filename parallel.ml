let nproc = ref 1

let set_nproc i = 
  if (i < 1) then
    failwith "Nproc has to be > 0"
  else if (i > 8192) then
    failwith "Nproc has to be < 8192"
  else
    nproc := i



let spawn ~f x =
  let input, output = 
    Unix.pipe ()
  in
  match Unix.fork () with
  | 0 -> 
    Unix.close input;
    let result =
      try `Ok (f x) with
      | e -> `Exn e
    in
    let output =
      Unix.out_channel_of_descr output
    in
    Marshal.to_channel output result [];
    close_out output;
    exit 0 ;
  | pid -> 
    Unix.close output;
    fun () -> 
      begin
        let input =
          Unix.in_channel_of_descr input
        in
        let result = 
          Marshal.from_channel input
        in
        ignore @@ Unix.waitpid [] pid;
        close_in input;
        match result with
        | `Ok x  -> x
        | `Exn e -> raise e
      end

let chunks size =
    let nproc = 
      !nproc
    in
    let chunk_size =
      max 1 (size/(nproc))
    in
    let rec loop accu = function
      | i when i <= 0 -> accu
      | i ->
          let new_accu =
             (max (i-chunk_size) 0, i) :: accu
          in
          loop new_accu (i-chunk_size)
    in
    loop [] size


let (array_sub, array_map, array_mapi, array_length, array_concat,
          array_iter) =
  Array.(sub,map,mapi,length,concat,iter)

module Array = struct

  let generic_map ~map_func ~f a =
      let slice (i,j) = 
        let pos, len =
          i, j-i
        in
        array_sub a pos len
        |> map_func f
      in
      chunks (array_length a)
      |> List.map (spawn ~f:slice)
      |> List.map (fun f -> f ())
      |> array_concat

  let map ~f a = 
    generic_map ~map_func:array_map ~f a

end

