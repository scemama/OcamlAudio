let () = 
  let input_name =
    Sys.argv.(1)
  in
  let file =
     Wav.File.read_exn input_name
  in
  let data =
    Wav.File.get_data file
  in
  match data with
  | Wav.Audio_data.MONO data ->
    for i=1 to 100000
    do
      print_int i;
      print_string " ";
      print_float data.(i);
      print_newline ();
    done
  | Wav.Audio_data.STEREO (left, right) ->
    for i=1 to 100000
    do
      print_int i;
      print_string " ";
      print_float left.(i);
      print_string " ";
      print_float right.(i);
      print_newline ();
    done
  | _ -> ()



