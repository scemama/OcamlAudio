(** Processing tools for {!Wav.Audio_data} *)

(** Process a file using a process function *)
val process_file :
  input_name:string ->
  process:(Wav.Audio_data.t -> Wav.Audio_data.t) ->
  output_name:string -> unit

(** Create a mono file by averaging channels *)
val convert_to_mono : Wav.Audio_data.t -> Wav.Audio_data.t

(** Create a stereo file from two mono channels *)
val stereo_of_two_mono : 
  left:Wav.Audio_data.t ->
  right:Wav.Audio_data.t -> 
  Wav.Audio_data.t 

(** Replace the sound of a metronome track *)
val replace_sound : reference:(Wav.Audio_data.t) -> 
  Wav.Audio_data.t ->
  Wav.Audio_data.t 


