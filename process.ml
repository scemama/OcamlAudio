let process_file ~input_name ~process ~output_name = 
  let in_file = 
    Wav.File.read_exn input_name
  in
  let data = 
    Wav.File.get_data in_file
    |> process
  in
  let out_channels, nframes =
    match data with
    | Wav.Audio_data.MONO    a        -> (Wav.Channels.MONO  , Array.length a)
    | Wav.Audio_data.STEREO (a,_)     -> (Wav.Channels.STEREO, Array.length a)
    | Wav.Audio_data.QUAD   (a,_,_,_) -> (Wav.Channels.QUAD  , Array.length a)
  in
  let out_file =
    Wav.File.(
      { name = output_name ;
        channels = out_channels ;
        frames = Wav.Num_frames.of_int nframes;
        sample_rate = in_file.sample_rate ;
        data = data
      }
    )
  in
  Wav.File.write out_file



let convert_to_mono = function
  | Wav.Audio_data.MONO a -> Wav.Audio_data.MONO a
  | Wav.Audio_data.STEREO (left, right) ->
        Wav.Audio_data.MONO (
          Array.mapi (fun i x -> 0.5 *. (x +. right.(i))) left
        )
  | Wav.Audio_data.QUAD (ch1, ch2, ch3, ch4) ->
        Wav.Audio_data.MONO (
          Array.mapi (fun i x -> 0.25 *. (x +. ch2.(i) +. ch3.(i) +. ch4.(i) )) ch1
        )

let stereo_of_two_mono ~left ~right =
  let left_data =
    match left with
    | Wav.Audio_data.MONO a -> a
    | _ -> failwith "Left channel is not MONO"
  and right_data =
    match right with
    | Wav.Audio_data.MONO a -> a
    | _ -> failwith "Right channel is not MONO"
  in
  let nframes_left = 
    Array.length left_data
  and nframes_right = 
    Array.length right_data
  in
  let nframes =
    max nframes_left nframes_right
  in
  let left, right = 
    if nframes_left < nframes_right then
      (Array.init nframes (fun i ->
        if i < nframes_left then
          left_data.(i)
        else
          0.
        ), right_data)
    else if nframes_left > nframes_right then
      (left_data,
       Array.init nframes (fun i ->
        if i < nframes_right then
          right_data.(i)
        else
          0. 
        ) )
    else
      (left_data, right_data)
  in
  Wav.Audio_data.STEREO (left, right)


let replace_sound ~reference data =
  let len x =
    match x with
    | Wav.Audio_data.MONO    a        -> Array.length a
    | Wav.Audio_data.STEREO (a,_)     -> Array.length a
    | Wav.Audio_data.QUAD   (a,_,_,_) -> Array.length a
  in
  let ref_size =
    len reference
  in
  let clicks =
      Trig.find_clicks ~reference ~width:1000 data
  in
List.iter (fun i -> Printf.printf "%d\n%!" i) clicks;
  let new_data_size =
    (List.rev clicks |> List.hd) + ref_size + 1
  in
  let result = 
    Array.make (new_data_size) 0.
  in
  let reference_data =
    match reference with
    | Wav.Audio_data.MONO    a -> a
    | _ -> failwith "Expected a MONO reference file"
  in
  List.iter (fun i -> 
    for j=0 to (ref_size-1)
    do
      result.(i+j) <- reference_data.(j)
    done) clicks;
  Wav.Audio_data.MONO result


