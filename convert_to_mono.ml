open Core

let run ?o input_name =

  let output_name =
    match o with
    | None -> 
      let basename = 
        let tmp_list = 
          String.split ~on:'.' input_name
          |> List.rev
        in
        let tmp_list = 
          match tmp_list with
          | "wav"::rest -> rest
          | _ -> failwith "Not a wav file"
        in
        List.rev tmp_list
        |> String.concat ~sep:"."
      in
      basename ^ ".mono.wav"
    | Some x -> x
  in
  Process.process_file ~input_name ~output_name
    ~process:Process.convert_to_mono



let () = 
  let doc = 
    "Converts a stereo wav file into a mono file."
  in
  let spec =
    let open Command.Spec in
    empty
    +> flag "o" (optional string)
       ~doc:("<WAV_FILE> name of the mono output file")
    +> anon ("wav_file" %: string)
  in
  Command.basic_spec
  ~summary:doc
  ~readme:(fun () -> "")
  spec
  (fun o wav_file () -> run ?o wav_file)
  |> Command.run


