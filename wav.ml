
module Num_frames = struct
  type t = Int64.t
  let to_int64 x = x
  let to_int = Int64.to_int 
  let to_string x = 
    to_int64 x
    |> Int64.to_string
  let of_int64 x = 
    if x >= 0L then
      x
    else
      failwith "Number of frames should be positive"
  let of_int x = 
    if x >= 0 then
      Int64.of_int x
    else
      failwith "Number of frames should be positive"
end

module Sample_rate = struct
  type t = int
  let to_int x = x
  let to_string x = 
    to_int x
    |> string_of_int
  let of_int x = 
    match x with
    | 11025 
    | 22050 
    | 44100 
    | 48000 
    | 96000 -> x
    | _ ->
        begin
          Printf.sprintf "Sample rate %d not allowed\n" x 
          |> failwith
        end
end

module Channels = struct
  type t = MONO | STEREO | QUAD

  let of_int = function
    | 1 -> MONO
    | 2 -> STEREO
    | 4 -> QUAD
    | x -> 
        Printf.sprintf "Number of channels (%d) not handled" x
        |> failwith
  let to_int = function
    | MONO   -> 1
    | STEREO -> 2
    | QUAD   -> 4
  let to_string = function
    | MONO   -> "mono"
    | STEREO -> "stereo"
    | QUAD   -> "quad"
end


module Audio_data = struct
  type t =
    | MONO   of float array
    | STEREO of float array * float array
    | QUAD   of float array * float array * float array * float array

  let of_float_arrays ~channels ~float_arrays =
    match channels with
    | Channels.MONO   -> MONO float_arrays.(0)
    | Channels.STEREO ->
        let left  = float_arrays.(0)
        and right = float_arrays.(1)
        in
        STEREO ( left, right )
    | Channels.QUAD -> 
        let ch1 = float_arrays.(0)
        and ch2 = float_arrays.(1)
        and ch3 = float_arrays.(2)
        and ch4 = float_arrays.(3)
        in
        QUAD (ch1, ch2, ch3, ch4)

  let to_float_arrays = function
    | MONO data -> [| data |]
    | STEREO (left, right) ->  [| left ; right |]
    | QUAD (ch1, ch2, ch3, ch4) -> [| ch1 ; ch2 ; ch3 ; ch4 |]

  let empty ~channels =
    of_float_arrays ~channels ~float_arrays:[| [| |] |]

end

module File = struct
  type t =
    {
      name        : string       ;
      sample_rate : Sample_rate.t ;
      channels    : Channels.t   ;
      frames      : Num_frames.t ;
      data        : Audio_data.t ;
    }

  let read_exn ~filename = 
    let f = new Audio.IO.Reader.of_wav_file filename in
    let blen = f#duration in
    let buf  = Audio.create f#channels blen in
    ignore @@  f#read buf 0 blen;
    let nframes = Num_frames.of_int blen in
    let sample_rate = f#sample_rate in
    let channels = Channels.of_int f#channels in
    let data = 
      Audio_data.of_float_arrays ~channels:channels ~float_arrays:buf
    in
    let result =
      { name        = filename ;
        sample_rate = sample_rate ;
        channels    = channels ;
        frames      = nframes  ;
        data        = data
      }
    in
    f#close;
    result

  let read ~filename =
    try
      Some (read_exn ~filename)
    with _ -> None

  let write file =
    let { name ; sample_rate ; channels ; frames ; data } =
      file
    in
    let f = new Audio.IO.Writer.to_wav_file 
      (Channels.to_int channels)
      (Sample_rate.to_int sample_rate)
      name
    in
    let buf = 
      match data with
      | Audio_data.MONO a -> [| a |]
      | Audio_data.STEREO (a,b) -> [| a ; b |]
      | Audio_data.QUAD   (a,b,c,d) -> [| a ; b ; c ; d |]
    in
    f#write buf 0 (Int64.to_int frames)


  let get_data { name ; channels ; frames ; sample_rate ; data } = 
    data

  let set_data { name ; channels ; frames ; sample_rate ; data } ~data:new_data =
    let new_frames = 
      match new_data with
      | Audio_data.MONO a -> Array.length a
      | Audio_data.STEREO (a,_) -> Array.length a
      | Audio_data.QUAD (a,_,_,_) -> Array.length a
    in
    { name ; channels ; frames = Num_frames.of_int new_frames ; sample_rate ; data = new_data }

  let get_name { name ; channels ; frames ; sample_rate ; data } = 
    name

  let set_name { name ; channels ; frames ; sample_rate ; data } ~name:new_name =
    { name = new_name ; channels ; frames ; sample_rate ; data }

  let print_info { name ; channels ; frames ; sample_rate ; data } = 
    Printf.printf "Name     : %s
Channels : %s
Frames   : %s
Sample_rate : %s
" name 
  (Channels.to_string channels)
  (Num_frames.to_string frames)
  (Sample_rate.to_string sample_rate)
end

