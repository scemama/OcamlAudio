open Core

let run ?n input_name ref_name output_name =
  Printf.printf "Input : %s\nOutput : %s\nReplace : %s\n%!"
    input_name output_name ref_name;
  let () = 
    match n with
    | Some i -> Parallel.set_nproc i
    | None -> ()
  in

  let file_in, file_ref =
     Wav.File.read_exn input_name, 
     Wav.File.read_exn ref_name 
  in
  let data, ref =
    Wav.File.get_data file_in, 
    Wav.File.get_data file_ref
  in
  let new_data = 
    Process.replace_sound ~reference:ref data
  in
  let new_file = 
    Wav.File.(
      set_data ~data:new_data file_in 
      |> set_name ~name:output_name
    )
  in
  Wav.File.write new_file


let () =
  let doc =
    "Replaces the click sound of a metronome track"
  in
  let spec =
    let open Command.Spec in
    empty
    +> flag "n" (optional int)
       ~doc:"<int> number of processes"
    +> flag "i" (required string)
       ~doc:"<WAV_FILE> input wav file"
    +> flag "r" (required string)
       ~doc:"<WAV_FILE> replacement click file"
    +> flag "o" (required string)
       ~doc:"<WAV_FILE> output wav file"
  in
  Command.basic_spec
  ~summary:doc
  ~readme:(fun () ->
    Printf.sprintf "
Example:
  %s -i mysong.click.wav -r click_sound.wav -o mysong.new_click.wav
  " Sys.argv.(0) )
  spec
  (fun n i r o () -> run ?n i r o )
  |> Command.run

