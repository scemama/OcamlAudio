.NOPARALLEL:

NAME=ocamlaudio
LIBS=
PKGS=
OCAMLCFLAGS="-g -warn-error A "
OCAMLLFLAGS=$(OCAMLCFLAGS) 
OCAMLBUILD=ocamlbuild -j 0 -cflags $(OCAMLCFLAGS) -lflags $(OCAMLLFLAGS) 
MLFILES=$(wildcard *.ml) 
MLIFILES=$(wildcard *.mli) 
ALL_TESTS=$(patsubst %.ml,%.byte,$(wildcard test_*.ml))
ALL_EXE=convert_to_mono.native convert_to_stereo.native replace_click.native 

.PHONY: executables default remake_executables


default: $(ALL_TESTS) $(ALL_EXE) 

%.odocl: $(MLIFILES)
	ls $(MLIFILES) | sed "s/\.mli//" > $*.odocl

doc: $(NAME).odocl
	$(OCAMLBUILD) $(NAME).docdir/index.html -use-ocamlfind  $(PKGS) 

%.inferred.mli: $(MLFILES)
	$(OCAMLBUILD) $*.inferred.mli -use-ocamlfind  $(PKGS) 
	mv _build/$*.inferred.mli .

%.byte: $(MLFILES) $(MLIFILES)
	rm -f -- $*
	$(OCAMLBUILD) $*.byte  -use-ocamlfind  $(PKGS)
	ln -s $*.byte $*

qp_run.native: $(MLFILES) $(MLIFILES) 

%.native: $(MLFILES) $(MLIFILES)
	rm -f -- $*
	$(OCAMLBUILD) $*.native -use-ocamlfind $(PKGS)
	ln -s $*.native $*


clean: 
	rm -rf _build $(ALL_EXE) $(ALL_TESTS)

